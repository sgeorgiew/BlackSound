﻿using System;
using BlackSound.Services;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public sealed class LoginView : IShow
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("### Login ###");

                Console.Write("Email: ");
                string email = Console.ReadLine();

                Console.Write("Password: ");
                string password = Console.ReadLine();

                AuthenticationService.Authenticate(email, password);

                if (AuthenticationService.LoggedUser != null)
                {
                    Console.WriteLine("\nWelcome, {0}! Your rank is: {1}!", 
                        AuthenticationService.LoggedUser.DisplayName, 
                        AuthenticationService.LoggedUser.IsAdmin ? "Admin" : "User");

                    Console.Title += " | Logged as: " + AuthenticationService.LoggedUser.Email + " |";

                    Console.Write("\nPress any key to continue");
                    Console.ReadKey(true);
                    break;
                }
                else
                {
                    ConsoleMessage.ShowError("\nInvalid email address or password!");
                }
            }
        }
    }
}
