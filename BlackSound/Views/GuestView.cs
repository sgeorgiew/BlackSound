﻿using System;
using BlackSound.Helpers;
using BlackSound.Services;
using BlackSound.Tools;

namespace BlackSound.Views
{
    public sealed class GuestView : IShow
    {
        public void Show()
        {
            Console.Clear();
            GuestEnum choice = RenderMenu();

            try
            {
                switch (choice)
                {
                    case GuestEnum.Login:
                        LoginView loginView = new LoginView();
                        loginView.Show();
                        break;

                    case GuestEnum.Register:
                        RegisterView registerView = new RegisterView();
                        registerView.Show();
                        break;

                    case GuestEnum.Exit:
                        return;

                    default:
                        ConsoleMessage.ShowError("Wrong choice!");
                        break;
                }
            }
            catch (Exception ex)
            {
                ConsoleMessage.ShowError(ex.Message);
            }

            if (AuthenticationService.LoggedUser == null) return;
            
            if (AuthenticationService.LoggedUser.IsAdmin)
            {
                AdminView adminView = new AdminView();
                adminView.Show();
            }
            else
            {
                PlaylistManagementView playlistView = new PlaylistManagementView();
                playlistView.Show();
            }
        }

        private GuestEnum RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                ConsoleMessage.Show("##### Welcome to BlackSound " + Constants.ApplicationVersion.Substring(0, 3) + " #####\n", ConsoleColor.DarkCyan);

                Console.WriteLine("Please select an option:");

                Console.WriteLine("[L]ogin");
                Console.WriteLine("[R]egister");
                Console.WriteLine("E[x]it");
                Console.Write("\n> ");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "L":
                        return GuestEnum.Login;

                    case "R":
                        return GuestEnum.Register;

                    case "X":
                        return GuestEnum.Exit;

                    default:
                        ConsoleMessage.ShowError("Invalid choice! Press any key to try again.");
                        break;
                }
            }
        }

    }
}
