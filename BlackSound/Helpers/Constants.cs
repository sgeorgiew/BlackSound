﻿using System;

namespace BlackSound.Helpers
{
    public static class Constants
    {
        public const string UsersFilePath = "../../Data/users.txt";
        public const string SongsFilePath = "../../Data/songs.txt";
        public const string PlaylistsFilePath = "../../Data/playlists.txt";
        public const string UserPlaylistsFilePath = "../../Data/user-playlists.txt";
        public const string PlaylistSongsFilePath = "../../Data/playlist-songs.txt";
        public const string ArtistsFilePath = "../../Data/artists.txt";

        public static readonly string ApplicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }
}
