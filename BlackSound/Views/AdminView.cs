﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public sealed class AdminView : IShow
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("##### BlackSound Admin Panel #####");
                Console.WriteLine("[U]ser Management");
                Console.WriteLine("[S]ongs Management");
                Console.WriteLine("[A]rtists Management");
                Console.WriteLine("[P]laylists Management");
                Console.WriteLine("E[x]it");
                Console.Write("\n> ");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "U":
                        UserManagementView userManView = new UserManagementView();
                        userManView.Show();
                        break;

                    case "S":
                        SongManagementView songManView = new SongManagementView();
                        songManView.Show();
                        break;

                    case "A":
                        ArtistManagementView artistManView = new ArtistManagementView();
                        artistManView.Show();
                        break;

                    case "P":
                        PlaylistManagementView playlistManView = new PlaylistManagementView();
                        playlistManView.Show();
                        break;

                    case "X":
                        return;

                    default:
                        ConsoleMessage.ShowError("Invalid choice! Press any key to try again.");
                        break;
                }
            }
        }
    }
}
