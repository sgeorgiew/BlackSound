﻿using System;
using BlackSound.Entities;
using BlackSound.Helpers;

namespace BlackSound.Repositories
{
    public class UserPlaylistRepository : BaseRepository<UserPlaylist>
    {
        public UserPlaylistRepository(string filePath = Constants.UserPlaylistsFilePath) : base(filePath)
        { }
    }
}
