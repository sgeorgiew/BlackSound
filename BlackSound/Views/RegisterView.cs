﻿using System;
using BlackSound.Entities;
using BlackSound.Repositories;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public sealed class RegisterView : IShow
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("### Register ###");

                Console.Write("Email: ");
                string email = Console.ReadLine();

                Console.Write("Password: ");
                string password = Console.ReadLine();

                Console.Write("Display Name: ");
                string displayName = Console.ReadLine();

                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(displayName))
                {
                    UserRepository userRepo = new UserRepository();

                    if (!userRepo.CheckUserExists(email, displayName))
                    {
                        User user = new User()
                        {
                            Email = email,
                            Password = password,
                            DisplayName = displayName,
                            IsAdmin = false
                        };

                        userRepo.Save(user);

                        ConsoleMessage.Show("\nYou are registered succesfully!\n", ConsoleColor.DarkGreen);
                        Console.Write("\nPress any key to continue.");
                        Console.ReadKey(true);

                        GuestView guestView = new GuestView();
                        guestView.Show();

                        break;
                    }

                    ConsoleMessage.ShowError("\nThere are already an user with this email or display name!\n");
                }
                else
                {
                    ConsoleMessage.ShowError("\nPlease fill all fields! \nPress any key to try again\n");
                }
            }
        }
    }
}
