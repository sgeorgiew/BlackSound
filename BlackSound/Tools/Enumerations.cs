﻿using System;

namespace BlackSound.Tools
{
    public enum BaseViewEnum
    {
        List,
        View,
        Add,
        Edit,
        Delete,
        Exit
    }

    public enum GuestEnum
    {
        Login,
        Register,
        Exit
    }
}
