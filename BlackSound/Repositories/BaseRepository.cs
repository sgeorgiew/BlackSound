﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;

namespace BlackSound.Repositories
{
    public abstract class BaseRepository<T> where T : Entities.IEntity, new()
    {
        protected readonly string filePath;

        public BaseRepository(string filePath)
        {
            this.filePath = filePath;
        }
        
        #region Public Methods

        public T GetByID(int id)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        List<T> result = DeserializeList(sr.ReadToEnd());
                        T temp = result.FirstOrDefault(s => s.ID == id);

                        return temp;
                    }
                }
            }

            return default(T);
        }

        public List<T> GetAll(Predicate<T> predicate = null)
        {
            List<T> result = new List<T>();

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        foreach (var item in DeserializeList(sr.ReadToEnd()))
                        {
                            if (predicate == null || predicate(item) == true)
                            {
                                result.Add(item);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public void Save(T item)
        {
            if (item.ID == 0)
            {
                item.ID = GetNextID();
            }

            using (FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                StreamReader sr = new StreamReader(ifs);

                try
                {
                    if (sr.EndOfStream)
                    {
                        List<T> itemSrc = DeserializeList(sr.ReadToEnd());

                        itemSrc.Add(item);
                        string jsonString = SerializeList(itemSrc);

                        sr.Close();

                        using (FileStream ofs = new FileStream(filePath, FileMode.OpenOrCreate))
                        {
                            using (StreamWriter sw = new StreamWriter(ofs))
                            {
                                sw.Write(jsonString);
                            }
                        }
                    }
                    else
                    {
                        while (!sr.EndOfStream)
                        {
                            List<T> itemSrc = DeserializeList(sr.ReadToEnd());

                            T temp = itemSrc.FirstOrDefault(s => s.ID == item.ID);

                            if (temp == null)
                            {
                                itemSrc.Add(item);
                            }
                            else
                            {
                                int index = itemSrc.IndexOf(temp);
                                itemSrc[index] = item;
                            }

                            string jsonString = SerializeList(itemSrc);
                            sr.Close();

                            using (FileStream ofs = new FileStream(filePath, FileMode.OpenOrCreate))
                            {
                                using (StreamWriter sw = new StreamWriter(ofs))
                                {
                                    sw.Write(jsonString);
                                }
                            }

                            return;
                        }
                    }
                }
                finally
                {
                    sr.Close();
                }
            }
        }

        public virtual bool Delete(T item)
        {
            bool isDeleted = false;

            using (FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                StreamReader sr = new StreamReader(ifs);

                try
                {
                    while (!sr.EndOfStream)
                    {
                        List<T> itemsSrc = DeserializeList(sr.ReadToEnd());
                        T temp = itemsSrc.FirstOrDefault(x => x.ID == item.ID);

                        if (temp != null)
                        {
                            var index = itemsSrc.IndexOf(temp);

                            itemsSrc.RemoveAt(index);

                            isDeleted = true;

                            string jsonString = SerializeList(itemsSrc);

                            sr.Close();

                            using (FileStream ofs = new FileStream(filePath, FileMode.Create))
                            {
                                using (StreamWriter sw = new StreamWriter(ofs))
                                {
                                    sw.Write(jsonString);
                                }
                            }

                            break;
                        }
                    }
                }
                finally
                {
                    sr.Close();
                }
            }

            return isDeleted;
        }

        #endregion

        #region Private Methods

        private int GetNextID()
        {
            int id = 1;

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        List<T> items = DeserializeList(sr.ReadToEnd());
                        if (items.Count > 0)
                        {
                            T lastItem = items[items.Count - 1];
                            id = lastItem.ID + 1;
                        }
                    }
                }
            }

            return id;
        }

        #region Serialization & Deserialization
        private string SerializeList(List<T> items)
        {
            string result = "";

            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer dataSerializer = new DataContractJsonSerializer(typeof(List<T>));
                dataSerializer.WriteObject(stream, items);
                stream.Position = 0;

                using (StreamReader reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        private List<T> DeserializeList(string json)
        {
            List<T> deserializedItem = new List<T>();
            if (!String.IsNullOrEmpty(json))
            {
                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    DataContractJsonSerializer dataSerializer = new DataContractJsonSerializer(deserializedItem.GetType());
                    deserializedItem = dataSerializer.ReadObject(ms) as List<T>;
                }
            }

            return deserializedItem;
        }
        #endregion

        #endregion

    }
}