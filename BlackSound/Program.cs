﻿using System;
using BlackSound.Views;
using BlackSound.Helpers;

namespace BlackSound
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "BlackSound v" + Constants.ApplicationVersion.Substring(0, 3);

            GuestView guestView = new GuestView();
            guestView.Show();
        }
    }
}
