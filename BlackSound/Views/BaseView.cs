﻿using System;
using System.Collections.Generic;
using BlackSound.Helpers;
using BlackSound.Repositories;
using BlackSound.Tools;

namespace BlackSound.Views
{
    public abstract class BaseView<T> where T : Entities.IEntity, new()
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();
                BaseViewEnum choice = RenderMenu();

                try
                {
                    switch (choice)
                    {
                        case BaseViewEnum.List:
                            GetAll();
                            break;
                        case BaseViewEnum.View:
                            View();
                            break;
                        case BaseViewEnum.Add:
                            Add();
                            break;
                        case BaseViewEnum.Edit:
                            Update();
                            break;
                        case BaseViewEnum.Delete:
                            Delete();
                            break;
                        case BaseViewEnum.Exit:
                            return;
                        default:
                            ConsoleMessage.ShowError("Wrong choice!");
                            Console.ReadKey(true);
                            break;
                    }
                }
                catch (ArgumentNullException argumentNullEx)
                {
                    ConsoleMessage.ShowError(argumentNullEx.ParamName);
                }
                catch (FormatException formatEx)
                {
                    ConsoleMessage.ShowError(formatEx.Message);
                }
                catch (Exception ex)
                {
                    ConsoleMessage.ShowError(ex.Message);
                }
            }
        }

        private BaseViewEnum RenderMenu()
        {
            Type type = typeof(T);

            while (true)
            {
                Console.Clear();
                Console.WriteLine("### {0} Management ###", type.Name);
                Console.WriteLine(new string('=', 25));
                Console.WriteLine("[G]et all {0}s", type.Name);
                Console.WriteLine("[V]iew {0}", type.Name);
                Console.WriteLine("[E]dit {0}", type.Name);
                Console.WriteLine("[A]dd {0}", type.Name);
                Console.WriteLine("[D]elete {0}", type.Name);
                Console.WriteLine("E[x]it");
                Console.Write("\n> ");

                string choice = Console.ReadLine();

                switch (choice.ToUpper())
                {
                    case "G":
                        return BaseViewEnum.List;

                    case "V":
                        return BaseViewEnum.View;

                    case "E":
                        return BaseViewEnum.Edit;

                    case "A":
                        return BaseViewEnum.Add;

                    case "D":
                        return BaseViewEnum.Delete;

                    case "X":
                        return BaseViewEnum.Exit;

                    default:
                        ConsoleMessage.ShowError("Invalid choice.");
                        break;
                }
            }
        }

        protected abstract BaseRepository<T> CreateRepository();
        protected abstract void RenderItem(T item);
        protected abstract void UpdateOrAddItem(T item);

        protected virtual void AdditionalUpdateOrDeleteLogic(T item)
        { }

        protected virtual List<T> GetAllItems(out string messageToShow)
        {
            Type type = typeof(T);
            messageToShow = $"All {type.Name}s";

            BaseRepository<T> repository = this.CreateRepository();
            List<T> items = repository.GetAll();

            return items;
        }

        private void GetAll()
        {
            Console.Clear();

            string messageToShow;
            List<T> items = GetAllItems(out messageToShow);

            Type type = typeof(T);

            if (items.Count < 1)
            {
                throw new Exception($"No {type.Name}s found!");
            }

            Console.WriteLine($"### {messageToShow} ###");
            Console.WriteLine(new string('=', 25));

            foreach (T item in items)
            {
                RenderItem(item);
            }

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        protected virtual void View()
        {
            Console.Clear();

            Type type = typeof(T);
            BaseRepository<T> repository = this.CreateRepository();

            Console.Write("Enter {0} ID: ", type.Name);
            string inputID = Console.ReadLine();

            if (!int.TryParse(inputID, out int id))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            T itemToView = repository.GetByID(id);

            if (itemToView == null)
            {
                throw new NullReferenceException($"{type.Name} does not exist with that id.");
            }

            Console.WriteLine();
            RenderItem(itemToView);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void Add()
        {
            Console.Clear();

            BaseRepository<T> repository = this.CreateRepository();

            T itemToAdd = new T();
            Type type = itemToAdd.GetType();

            Console.WriteLine($"### Add new {type.Name} ###");
            UpdateOrAddItem(itemToAdd);

            repository.Save(itemToAdd);
            ConsoleMessage.Show("Added successfully!\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void Update()
        {
            Console.Clear();
            Type type = typeof(T);

            Console.Write("Enter {0} ID to edit: ", type.Name);
            string inputID = Console.ReadLine();

            if (!int.TryParse(inputID, out int id))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            Console.WriteLine(new string('=', 25));

            BaseRepository<T> repository = this.CreateRepository();
            T itemToUpdate = repository.GetByID(id);

            if (itemToUpdate == null)
            {
                throw new NullReferenceException($"{type.Name} does not exist with that id.");
            }

            AdditionalUpdateOrDeleteLogic(itemToUpdate);

            ConsoleMessage.Show("\n# Note: If you don't want to change something, leave it empty\n\n", ConsoleColor.DarkCyan);

            Console.WriteLine("## Current data ##");

            RenderItem(itemToUpdate);
            UpdateOrAddItem(itemToUpdate);

            repository.Save(itemToUpdate);

            ConsoleMessage.Show("Updated successfully!\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void Delete()
        {
            Console.Clear();
            Type type = typeof(T);
            BaseRepository<T> repository = this.CreateRepository();

            Console.Write("Enter {0} Id: ", type.Name);
            string inputID = Console.ReadLine();

            if (!int.TryParse(inputID, out int id))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            T itemToDelete = repository.GetByID(id);

            if (itemToDelete == null)
            {
                throw new NullReferenceException($"{type.Name} not found!");
            }

            AdditionalUpdateOrDeleteLogic(itemToDelete);

            repository.Delete(itemToDelete);

            ConsoleMessage.Show($"{type.Name} is deleted successfully!\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        protected void ValidateEntity(T item)
        {
            var entityProperties = item.GetType().GetProperties();
            foreach (var property in entityProperties)
            {
                if (string.IsNullOrEmpty(property.GetValue(item).ToString()))
                {
                    throw new ArgumentNullException($"The {property.Name.ToLower()} can't be empty!");
                }
            }
        }

    }
}
