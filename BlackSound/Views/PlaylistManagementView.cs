﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlackSound.Entities;
using BlackSound.Repositories;
using BlackSound.Services;
using BlackSound.Helpers;

namespace BlackSound.Views
{
    public sealed class PlaylistManagementView : BaseView<Playlist>
    {
        protected override BaseRepository<Playlist> CreateRepository()
        {
            return new PlaylistRepository();
        }

        protected override void RenderItem(Playlist playlist)
        {
            int loggedUserID = AuthenticationService.LoggedUser.ID;
            string loggedUserDisplayName = AuthenticationService.LoggedUser.DisplayName;

            UserRepository userRepo = new UserRepository();
            User user = userRepo.GetByID(playlist.UserID);
            
            Console.WriteLine("ID: {0}", playlist.ID);
            Console.WriteLine("Name: {0}", playlist.Name);
            Console.WriteLine("Description: {0}", playlist.Description);
            Console.WriteLine("Playlist Owner: {0}", user.DisplayName == loggedUserDisplayName ? "You" : user.DisplayName);

            if (playlist.UserID == loggedUserID)
            {
                Console.WriteLine("IsPublic: {0}", playlist.IsPublic);

                if (!playlist.IsPublic)
                {
                    Console.Write("Shared with: ");

                    List<User> sharedWithUsers = playlist.SharedWith();

                    int counter = 0;
                    foreach (User usr in sharedWithUsers)
                    {
                        ConsoleMessage.Show($"{usr.DisplayName}", ConsoleColor.Cyan);
                        
                        if (counter != sharedWithUsers.Count - 1)
                        {
                            Console.Write(", ");
                        }

                        counter++;
                    }

                    if (sharedWithUsers.Count < 1)
                    {
                        Console.Write("None");
                    }

                    Console.WriteLine();
                }
            }
            else
            {
                ConsoleMessage.Show("-> This playlist is public or shared with you\n", ConsoleColor.Yellow);
            }

            Console.WriteLine(new string('=', 25));
        }

        protected override void UpdateOrAddItem(Playlist playlist)
        {
            if (playlist.ID != 0)
            {
                Console.Write("New Name: ");
                string name = Console.ReadLine();

                Console.Write("New Description: ");
                string description = Console.ReadLine();

                Console.Write("Is public playlist? (True/False): ");
                string isPublicPlaylist = Console.ReadLine();

                if (!string.IsNullOrEmpty(name))
                {
                    playlist.Name = name;
                }

                if (!string.IsNullOrEmpty(description))
                {
                    playlist.Description = description;
                }

                if (!bool.TryParse(isPublicPlaylist, out bool isPublic))
                {
                    throw new FormatException("Incorrect input! Please enter true or false.");
                }

                playlist.IsPublic = isPublic;
            }
            else
            {
                Console.Write("Name: ");
                playlist.Name = Console.ReadLine();

                Console.Write("Description: ");
                playlist.Description = Console.ReadLine();

                Console.Write("Is public playlist? (True/False): ");
                string isPublicPlaylist = Console.ReadLine();

                if (!bool.TryParse(isPublicPlaylist, out bool isPublic))
                {
                    throw new FormatException("Incorrect input! Please enter true or false.");
                }

                playlist.IsPublic = isPublic;
                playlist.UserID = AuthenticationService.LoggedUser.ID;

                Console.WriteLine(new string('=', 25));

                ValidateEntity(playlist);
            }
        }

        protected override List<Playlist> GetAllItems(out string messageToShow)
        {
            messageToShow = "All playlists (owned, shared with you or public)";

            int loggedUserID = AuthenticationService.LoggedUser.ID;
            PlaylistRepository repository = new PlaylistRepository();
            List<Playlist> playlists = repository.GetAll(x => x.IsSharedWith(loggedUserID));

            return playlists;
        }

        protected override void AdditionalUpdateOrDeleteLogic(Playlist playlist)
        {
            int loggedUserID = AuthenticationService.LoggedUser.ID;

            if (playlist.UserID != loggedUserID)
            {
                throw new Exception("You can not edit or delete this playlist! It is not yours.");
            }
        }

        protected override void View()
        {
            Console.Clear();

            int loggedUserID = AuthenticationService.LoggedUser.ID;
            PlaylistRepository repository = new PlaylistRepository();
            List<Playlist> playlists = repository.GetAll(x => x.IsSharedWith(loggedUserID));

            if (playlists.Count < 1)
            {
                throw new Exception("There are no playlists for you! Try to create your own playlist.");
            }

            Console.Write("Enter playlist ID: ");
            string inputPlaylistID = Console.ReadLine();

            if (!int.TryParse(inputPlaylistID, out int playlistID))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            Playlist itemToView = repository.GetByID(playlistID);

            if (itemToView == null)
            {
                throw new NullReferenceException("Playlist does not exist with that id.");
            }

            if (!itemToView.IsSharedWith(loggedUserID))
            {
                throw new Exception("You don't have access for this playlist!");
            }

            PlaylistSongRenderMenu(itemToView);
        }
        
        private void PlaylistSongRenderMenu(Playlist playlist)
        {
            while (true)
            {
                Console.Clear();

                RenderItem(playlist);

                Console.WriteLine("\n[V]iew all songs in this playlist");

                int loggedUserID = AuthenticationService.LoggedUser.ID;

                if (playlist.UserID == loggedUserID)
                {
                    Console.WriteLine("[A]dd song");
                    Console.WriteLine("[R]emove song");
                    Console.WriteLine("[S]hare this playlist");
                }

                Console.WriteLine("E[x]it");
                Console.Write("\n> ");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "V":
                        GetAllSongs(playlist);
                        break;

                    case "A":
                        if (playlist.UserID == loggedUserID)
                        {
                            AddSong(playlist);
                        }
                        break;

                    case "R":
                        if (playlist.UserID == loggedUserID)
                        {
                            RemoveSong(playlist);
                        }
                        break;

                    case "S":
                        if (playlist.UserID == loggedUserID)
                        {
                            Share(playlist);
                        }
                        break;

                    case "X":
                        return;

                    default:
                        ConsoleMessage.ShowError("Invalid choice!");
                        break;
                }
            }
        }

        private void GetAllSongs(Playlist playlist)
        {
            Console.Clear();

            PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository();
            List<PlaylistSong> playlistSongs = playlistSongRepo.GetAll(x => x.PlaylistID == playlist.ID);

            if (playlistSongs.Count < 1)
            {
                throw new Exception("This playlist is empty.");
            }

            Console.WriteLine("### Songs in playlist - {0} ###", playlist.Name);

            foreach (PlaylistSong item in playlistSongs)
            {
                SongRepository songRepo = new SongRepository();
                Song song = songRepo.GetByID(item.SongID);
                Console.WriteLine("#{0} {1}", song.ID, song.Title);
            }

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void AddSong(Playlist playlist)
        {
            Console.Clear();

            SongRepository songRepo = new SongRepository();
            List<Song> songs = songRepo.GetAll();

            if (songs.Count < 1)
            {
                throw new Exception("There are no songs! Please try again later.");
            }
            
            ConsoleMessage.Show("### Available Songs ###\n\n", ConsoleColor.DarkCyan);

            foreach (Song item in songs)
            {
                Console.WriteLine("#{0} {1}", item.ID, item.Title);
            }

            Console.WriteLine("\n*** Add a song to playlist {0} ***", playlist.Name);

            Console.Write("Song ID: ");
            string inputSong = Console.ReadLine();

            if (!int.TryParse(inputSong, out int songID))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            Song song = songRepo.GetByID(songID);
            if (song == null)
            {
                throw new NullReferenceException($"There is no song with id {songID}");
            }

            PlaylistSong playlistSong = new PlaylistSong();
            playlistSong.SongID = songID;
            playlistSong.PlaylistID = playlist.ID;
            
            PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository();
            playlistSongRepo.Save(playlistSong);

            ConsoleMessage.Show("Song is added successfully!\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void RemoveSong(Playlist playlist)
        {
            Console.Clear();

            PlaylistSongRepository playlistSongRepo = new PlaylistSongRepository();
            List<PlaylistSong> playlistSongs = playlistSongRepo.GetAll(x => x.PlaylistID == playlist.ID);

            if (playlistSongs.Count < 1)
            {
                throw new Exception("This playlist is empty.");
            }

            Console.WriteLine("*** Remove a song from playlist {0} ***", playlist.Name);
            Console.WriteLine("\n### Songs in playlist {0} ###", playlist.Name);

            SongRepository songRepo = new SongRepository();

            foreach (PlaylistSong item in playlistSongs)
            {
                Song song = songRepo.GetByID(item.SongID);
                Console.WriteLine("#{0} {1}", song.ID, song.Title);
            }

            Console.Write("Song ID: ");
            int songID = Convert.ToInt32(Console.ReadLine());

            Song enteredSong = songRepo.GetByID(songID);

            if (enteredSong == null || !playlistSongs.Any(x => x.SongID == songID))
            {
                throw new NullReferenceException($"There is no song with id {songID}");
            }

            PlaylistSong playlistSong = playlistSongs.First(x => x.SongID == songID);
            playlistSongRepo.Delete(playlistSong);

            ConsoleMessage.Show("Song is removed successfully.\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void Share(Playlist playlist)
        {
            Console.Clear();

            int loggedUserID = AuthenticationService.LoggedUser.ID;

            if (playlist.UserID != loggedUserID)
            {
                throw new Exception("You are not able to share this playlist!");
            }

            while (true)
            {
                Console.Clear();

                Console.WriteLine("*** Share Playlist {0} ***", playlist.Name);
                Console.WriteLine("[A]dd member");
                Console.WriteLine("[R]emove member");
                Console.WriteLine("[M]ake {0}", playlist.IsPublic ? "private" : "public");
                Console.WriteLine("E[x]it");
                Console.Write("\n> ");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "A":
                        if (playlist.IsPublic)
                        {
                            throw new Exception("This playlist is public! Everyone have access to it.");
                        }

                        AddMemberInPlaylist(playlist.ID);
                        break;

                    case "R":
                        if (playlist.IsPublic)
                        {
                            throw new Exception("This playlist is public! Everyone have access to it.");
                        }

                        RemoveMemberFromPlaylist(playlist.ID);
                        break;

                    case "M":
                        playlist.IsPublic = !playlist.IsPublic;

                        PlaylistRepository playlistRepo = new PlaylistRepository();
                        playlistRepo.Save(playlist);
                        break;

                    case "X":
                        return;

                    default:
                        ConsoleMessage.ShowError("Invalid choice!");
                        break;
                }
            }
        }

        private void AddMemberInPlaylist(int playlistID)
        {
            Console.Clear();

            int loggedUserID = AuthenticationService.LoggedUser.ID;
            UserRepository userRepo = new UserRepository();
            List<User> users = userRepo.GetAll(u => u.ID != loggedUserID);

            UserPlaylistRepository userPlaylistRepo = new UserPlaylistRepository();
            List<UserPlaylist> userPlaylists = userPlaylistRepo.GetAll(p => p.PlaylistID == playlistID);

            List<User> notSharedUsers = (from user in users
                                        join userPlaylis in userPlaylists
                                        on user.ID equals userPlaylis.UserID
                                        select user).ToList();

            foreach (User user in notSharedUsers)
            {
                users.Remove(user);
            }

            if (users.Count < 1)
            {
                throw new Exception("There are no users to share with.");
            }
            
            ConsoleMessage.Show("### Available Users ###\n\n", ConsoleColor.DarkCyan);

            foreach (User user in users)
            {
                Console.WriteLine("#{0} {1}", user.ID, user.DisplayName);
            }

            Console.WriteLine(new string('#', 20));
            Console.Write("\nShare with (enter the user ID): ");
            
            if (!int.TryParse(Console.ReadLine(), out int userID))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            if (!users.Any(x => x.ID == userID))
            {
                throw new Exception($"There is no user with ID {userID}.");
            }

            UserPlaylist userPlaylist = new UserPlaylist(playlistID, userID);
            userPlaylistRepo.Save(userPlaylist);

            ConsoleMessage.Show("Successfully added a member to your playlist.\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

        private void RemoveMemberFromPlaylist(int playlistID)
        {
            Console.Clear();

            UserPlaylistRepository userPlaylistRepo = new UserPlaylistRepository();
            var sharedUsers = userPlaylistRepo.GetAll(x => x.PlaylistID == playlistID);

            if (sharedUsers.Count < 1)
            {
                throw new Exception("This playlist is not shared with anyone.");
            }

            Console.WriteLine("### Shared with users ###");

            foreach (var sharedUser in sharedUsers)
            {
                User user = new UserRepository().GetByID(sharedUser.UserID);
                Console.WriteLine("#{0} {1}", user.ID, user.DisplayName);
            }

            Console.WriteLine(new string('#', 20));
            Console.Write("\nRemove user (enter the user ID): ");

            int userID = 0;

            if (!int.TryParse(Console.ReadLine(), out userID))
            {
                throw new FormatException("Incorrect input! Please enter a valid ID.");
            }

            UserPlaylist userPlaylist = userPlaylistRepo.GetAll(x => x.UserID == userID).FirstOrDefault();

            if (!sharedUsers.Any(x => x.UserID == userID))
            {
                throw new Exception($"This playlist is not shared with user with ID {userID}.");
            }

            userPlaylistRepo.Delete(userPlaylist);
            ConsoleMessage.Show("Successfully removed a user from your playlist.\n", ConsoleColor.DarkGreen);

            Console.Write("\nPress any key to go back");
            Console.ReadKey(true);
        }

    }
}
